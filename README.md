This project is about showing a list of products.

Folder structure:
 src
  │   App.js
  │   index.css
  │   index.js
  │   reportWebVitals.js
  │   setupTests.js
  │
  ├───assets  
  │   ├───images
  │   └───svgs
  │
  ├───components
  │   ├───Card
  │   ├───Header
  │   └───Modal
  │
  ├───service
  ├───utils
  └───views

assets = all static asset like images and svg's in react component
components = commonly used components which are easily reusable for pages
service = generally it contain api calls but for this project it is dealing with localstorage 
utils = normal js function which can be used at any part in our app
views = contain pages