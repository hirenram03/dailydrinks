export const getAllProduct = () => {
  return new Promise((resolve, reject) => {
    let products = localStorage.getItem("products");
    if (!products) {
      localStorage.setItem("products", JSON.stringify([]));
    } else {
      products = JSON.parse(products);
    }
    resolve(products || []);
  });
};

export const updateList = (newProductList) => {
  localStorage.setItem("products", JSON.stringify(newProductList));
};
