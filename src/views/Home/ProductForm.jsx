import { useEffect, useState } from "react";

import style from "./product_form.module.css";

import { isValidString, isValidPositiveNumber } from "utils/validators";

const ProductForm = ({
  closeModal,
  submitForm,
  name: nameP = "",
  price: priceP,
  note: noteP = "",
  ...rest
}) => {
  console.log("nameP", nameP, rest)
  const [name, setName] = useState(nameP);
  const [price, setPrice] = useState(priceP);
  const [note, setNote] = useState(noteP);

  useEffect(() => {}, []);

  const submitFormValidate = (e) => {
    e.preventDefault();
    if (
      isValidString(name) &&
      isValidString(note) &&
      isValidPositiveNumber(price)
    ) {
      submitForm({ name, price, note });
    } else {
      alert("Enter proper value in form");
    }
  };

  return (
    <div className={style["form-container"]}>
      <form onSubmit={(e) => submitFormValidate(e)}>
        <input
          name="name"
          onChange={(e) => setName(e.target.value)}
          placeholder="Name"
          value={name}
        />
        <input
          name="price"
          type="number"
          onChange={(e) => setPrice(e.target.value)}
          placeholder="price"
          value={price}
        />
        <textarea
          name="note"
          onChange={(e) => setNote(e.target.value)}
          placeholder="Note"
          value={note}
        />

        <div className={style["buttons"]}>
          <button onClick={closeModal}>Cancel</button>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
};

export default ProductForm;
