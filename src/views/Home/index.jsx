import Header from "components/Header";
import ProductList from "views/Home/ProductList";

const Home = () => {
  return <div>
    <Header />
    <ProductList />
  </div>;
};

export default Home;
