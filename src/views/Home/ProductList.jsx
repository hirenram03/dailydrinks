import { useState, useEffect } from "react";

import Modal from "components/Modal";
import ProductCard from "components/Card/ProductCard";
import ProductForm from "./ProductForm";

import styles from "./index.module.css";

import { getAllProduct, updateList } from "services/products";

const ProductList = () => {
  const [list, setList] = useState([]);
  const [addNewProductPopup, setAddNewProductPopup] = useState(false);
  const [editProductPopup, setEditProductPopup] = useState(false);

  useEffect(() => {
    getAllProduct().then((resp) => {
      console.log(resp);
      setList(resp);
    });
  }, []);

  const closeNewProductModal = (e) => {
    e.preventDefault();
    setAddNewProductPopup(false);
  };

  const addNewProductInList = (value) => {
    const updatedList = [...list];
    updatedList.push(value);
    updateList(updatedList);
    setAddNewProductPopup(false);
    setList(updatedList);
  };

  const closeEditProductModal = (e) => {
    e.preventDefault();
    setEditProductPopup(false);
  };

  const editNewProductInList = (value) => {
    const updatedList = [...list];
    updatedList[editProductPopup.id] = value;
    updateList(updatedList);
    setEditProductPopup(false);
    setList(updatedList);
  };

  const deleteProduct = (id) => {
    if (window.confirm("Are you sure you want to remove this product?")) {
      const updatedList = [...list];
      updatedList.splice(id, 1);
      updateList(updatedList);
      setList(updatedList);
    }
  };

  return (
    <div className={styles["productlist-bg"]}>
      <div className="container">
        <span
          onClick={(_) => setAddNewProductPopup(true)}
          className={styles["addtolist-btn"]}
        >
          Add to list
        </span>
        <div className={styles["product-list"]}>
          {list.map((item, index) => (
            <ProductCard
              deleteCard={(_) => deleteProduct(index)}
              editCard={(_) =>
                setEditProductPopup({
                  name: item.name,
                  note: item.note,
                  price: item.price,
                  id: index,
                })
              }
              {...item}
              key={`index-${index}`}
            />
          ))}
        </div>
      </div>
      {addNewProductPopup && (
        <Modal>
          <ProductForm
            submitForm={addNewProductInList}
            closeModal={closeNewProductModal}
          />
        </Modal>
      )}
      {editProductPopup && (
        <Modal>
          <ProductForm
            submitForm={editNewProductInList}
            closeModal={closeEditProductModal}
            {...editProductPopup}
          />
        </Modal>
      )}
    </div>
  );
};

export default ProductList;
