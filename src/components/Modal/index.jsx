import style from "./index.module.css";

const Modal = ({ children }) => {
  return (
    <div className={style["modal"]}>
      <div className={style["modal-content"]}>{children}</div>
    </div>
  );
};

export default Modal;
