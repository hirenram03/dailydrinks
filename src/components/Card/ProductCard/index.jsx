import style from "./index.module.css";
import DummyImage from "assets/images/product-img.png";
import Delete from "assets/svgs/Delete";
import Edit from "assets/svgs/Edit";

const ProductCard = ({
  productImage = DummyImage,
  name,
  price,
  note,
  editCard,
  deleteCard
}) => {
  return (
    <div className={style["product-boxs"]}>
      <div className={style["img"]}>
        <img src={productImage} alt="" />
      </div>
      <h3>
        {name} <span className={style["price"]}>${price}</span>
      </h3>
      <p>{note}</p>
      <div className={style["buttons"]}>
        <span
          onClick={(_) => editCard()}
          className={style["icon"]}
        >
          <Edit />
        </span>
        <span onClick={(_) => deleteCard()} className={style["icon"]}>
          <Delete />
        </span>
      </div>
    </div>
  );
};

export default ProductCard;
