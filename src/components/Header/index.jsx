import Logo from "assets/svgs/Logo";
import style from "./header.module.css";

const Header = () => {
  return (
    <header className={style["header"]}>
      <div className="container">
        <div className={style["header-boxs"]}>
          <span className={style["logo"]}>
            <Logo />
            <span>Daily Drinks</span>
          </span>
        </div>
      </div>
    </header>
  );
};

export default Header;
