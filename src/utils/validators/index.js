export const isValidString = (value) => value && value.trim().length !== 0;

export const isValidPositiveNumber = (value) => value && value > 0;
